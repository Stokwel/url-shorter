<?php

// configure your app for the production environment

//$app['twig.path'] = array(__DIR__.'/../templates');
//$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');

return [
    'sqllite3.config' => [
        'location' => __DIR__.'/../var/sqllite',
        'dbname' => 'urlShorter'
    ],
    'redis.config' => [
        'host' => '127.0.0.1'
    ]
];
