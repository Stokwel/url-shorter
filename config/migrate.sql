CREATE TABLE urls_map
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    url TEXT,
    hash TEXT
);
CREATE UNIQUE INDEX urls_map_hash_uindex ON urls_map (hash);