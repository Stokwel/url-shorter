<?php
namespace providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class RedisProvider extends BaseProvider implements ServiceProviderInterface
{ 
    protected $providerName = 'redis';
    protected $mandatoryConfigParams = ['host'];
    
    public function register(Container $app)
    {
        $this->checkConfig($app);

        $app['redis'] = function ($app) {
            $redis = new \Redis();
            $redis->connect($app[$this->getConfigName()]['host']);
            
            return $redis;
        };
    }
}