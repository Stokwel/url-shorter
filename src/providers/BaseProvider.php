<?php
namespace providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

abstract class BaseProvider implements ServiceProviderInterface
{
    protected $providerName = '';
    protected $mandatoryConfigParams = [];

    protected function checkConfig(Container $app)
    {
        if (empty($app[$this->getConfigName()])) {
            throw new \Exception($this->providerName.' error: config is not exist or empty!');
        }

        foreach ($this->mandatoryConfigParams as $param) {
            if (empty($app[$this->getConfigName()][$param])) {
                throw new \Exception($this->providerName.' error: "'.$param.'" does not defined in config!');
            }
        }
    }

    protected function getConfigName()
    {
        return $this->providerName.'.config';
    }
}