<?php
namespace providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class SQLite3Provider extends BaseProvider implements ServiceProviderInterface
{
    protected $providerName = 'sqllite3';
    protected $mandatoryConfigParams = ['location', 'dbname'];

    private function getDbPath(Container $app)
    {
        return $app[$this->getConfigName()]['location'].'/'.$app[$this->getConfigName()]['dbname'].'.sqlite';
    }

    public function register(Container $app)
    {
        $this->checkConfig($app);

        if (!is_dir($app[$this->getConfigName()]['location'])) {
            mkdir($app[$this->getConfigName()]['location']);
        }

        $app['sqllite3'] = function ($app) {
            return new \SQLite3($this->getDbPath($app), SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
        };
    }
}