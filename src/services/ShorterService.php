<?php
namespace services;

use Hashids\Hashids;
use Silex\Application;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ShorterService
{
    use Application\UrlGeneratorTrait;

    private $app;

    const CACHE_TTL = 3600;
    
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getHash($url)
    {
        return (new Hashids($url))->encode(1, 2, 3);
    }

    public function getShortUrl($hash)
    {
        return $this->app['url_generator']->generate('index', ['hash' => $hash], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public function giveShortUrl($url)
    {
        $hash = '';

        $stmt = $this->app['sqllite3']->prepare('SELECT hash FROM urls_map WHERE url=:url');
        $stmt->bindValue(':url', $url, SQLITE3_TEXT);
        $result = $stmt->execute();
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $hash = $row['hash'];
            break;
        }

        if ($hash) {
            return $this->getShortUrl($hash);
        }

        $hash = $this->getHash($url);
        $stmt = $this->app['sqllite3']->prepare('INSERT INTO urls_map (url, hash) VALUES (:url, :hash)');
        $stmt->bindValue(':url', $url, SQLITE3_TEXT);
        $stmt->bindValue(':hash', $hash, SQLITE3_TEXT);
        $result = $stmt->execute();

        if (!$result) {
            throw new \Exception('Something wrong!');
        }

        $this->app['redis']->set($hash, $url, self::CACHE_TTL);

        return $this->getShortUrl($hash);
    }

    public function getUrlByHash($hash)
    {
        $url = $this->app['redis']->get($hash);

        if ($url) {
            return $url;
        }

        $stmt = $this->app['sqllite3']->prepare('SELECT url FROM urls_map WHERE hash=:hash');
        $stmt->bindValue(':hash', $hash, SQLITE3_TEXT);
        $result = $stmt->execute();
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $url = $row['url'];
            break;
        }

        if (!$url) {
            throw new \Exception('Url not found!');
        }

        return $url;
    }
}