<?php
namespace controllers;


use services\ShorterService;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class ShorterController extends BaseController
{
    public function index($hash)
    {
        $shorter = new ShorterService($this->app);
        $url = $shorter->getUrlByHash($hash);

        return $this->app->redirect($url);
    }

    public function getShortUrl(Request $request)
    {
        $url = $request->request->get('url');
        
        if (!$url) {
            throw new Exception('Missing required parameter: url');
        }

        $shorter = new ShorterService($this->app);
        $shortUrl = $shorter->giveShortUrl($url);
        
        return [
            'shortUrl' => $shortUrl
        ];
    }
}