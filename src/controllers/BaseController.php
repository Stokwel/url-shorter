<?php
namespace controllers;

use Silex\Application;

abstract class BaseController
{
    /** @var  Application */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }
}