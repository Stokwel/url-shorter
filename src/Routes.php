<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Routes
{
    /** @var Application */
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
        $this->instantiateControllers();
    }
    
    private function instantiateControllers()
    {
        $this->app['shorter.controller'] = function() {
            return new \controllers\ShorterController($this->app);
        };
    }
    
    public function bindRoutesToControllers()
    {
        $this->app->get('/{hash}', 'shorter.controller:index')->assert('hash', '[0-9a-zA-Z]{0,6}')->bind('index');
        $this->app->post('/getShortUrl', 'shorter.controller:getShortUrl');
        
        $this->bindErrorHandler();
    }

    public function bindErrorHandler()
    {
        $this->app->error(function (\Exception $e, Request $request, $code) {
            if ($this->app['debug']) {
                return [];
            }

            return [
                'code' => $code,
                'message' => $e->getMessage()
            ];
        });
    }
}