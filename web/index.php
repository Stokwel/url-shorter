<?php
use Silex\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use \Symfony\Component\HttpFoundation\Request;
use \Silex\Provider\RoutingServiceProvider;
use \providers\SQLite3Provider;
use \providers\RedisProvider;

ini_set('display_errors', 0);

require_once __DIR__.'/../vendor/autoload.php';

$config = require __DIR__.'/../config/prod.php';
$app = new Application($config);

$app->register(new RoutingServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new SQLite3Provider());
$app->register(new RedisProvider());

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->view(function (array $controllerResult) use ($app) {
    return $app->json($controllerResult);
});

$routesLoader = new Routes($app);
$routesLoader->bindRoutesToControllers();

$app->run();
